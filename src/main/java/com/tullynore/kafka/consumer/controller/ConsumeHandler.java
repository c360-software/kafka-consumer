package com.tullynore.kafka.consumer.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConsumeHandler {

    private String topicName;
    private String groupId;

    public ConsumeHandler(@Value("${kafka.topic}") String topicName,
                          @Value("${kafka.group-id}") String groupId) {
        this.topicName = topicName;
        this.groupId = groupId;
    }

    @KafkaListener(topics = "outline_scan_topic", groupId = "outline-group")
    public void listenGroupFoo(String message) {
        System.out.println("Received Message in group foo: " + message);
    }
}
